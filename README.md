# Docker PHP QA Image

Docker image for PHP QA (Quality Assurance), and CI&CD.

## Git

- https://gitlab.com/morfcz/docker-php-qa

## Requirements

- docker
- packer.io

## Build

./build.sh 	to build docker images and push them to docker hub
./clean.sh 	to clean docker images (`*.tar`)

## List of PHP QA Tools

- /usr/local/bin/composer	composer
- /usr/local/bin/phpcs		php code sniffer
- /usr/local/bin/phpcbf		php code sniffer fixer
- /usr/local/bin/phpcpd 	php copy & paste detector
- /usr/local/bin/phpcpd143	php copy & paste detector version for php 5.x
- /usr/local/bin/phpdoc		php documentor
- /usr/local/bin/phpunit	php unit
- /usr/local/bin/phpmd		php mess detector

## Known Issues

- latest phpcpd versions doesn't run @ PHP 7.x ; using phpcpd 1.4.3 for PHP 5.x

## Version history

- `1.0.0`: initial version
