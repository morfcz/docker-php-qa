#!/bin/bash

#
# Build docker images used for ci/cd using docker.
#
# use PACKER_LOG=1 env for debug
# change docker hub repository if needed to your own
#

packer-io build -var-file 5.json template.json
packer-io build -var-file 7.json template.json
docker push morfxyz/php-qa
