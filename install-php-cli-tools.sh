#!/bin/sh

#
# Shell script to install php cli tools for automated testing, code analyses and documentation.
#

set -e
set -x

curl --location --output /usr/local/bin/composer https://github.com/composer/composer/releases/download/1.6.5/composer.phar
curl --location --output /usr/local/bin/phpcs https://squizlabs.github.io/PHP_CodeSniffer/phpcs.phar
curl --location --output /usr/local/bin/phpcbf https://squizlabs.github.io/PHP_CodeSniffer/phpcbf.phar
curl --location --output /usr/local/bin/phpcpd-1.4.3.phar https://phar.phpunit.de/phpcpd-1.4.3.phar
curl --location --output /usr/local/bin/phpcpd https://phar.phpunit.de/phpcpd.phar
curl --location --output /usr/local/bin/phpdoc https://github.com/phpDocumentor/phpDocumentor2/releases/download/v2.9.0/phpDocumentor.phar
curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
curl --location --output /usr/local/bin/phpmd http://static.phpmd.org/php/latest/phpmd.phar

chmod +x /usr/local/bin/composer \
	/usr/local/bin/phpcs \
	/usr/local/bin/phpcbf \
	/usr/local/bin/phpcpd \
	/usr/local/bin/phpcpd-1.4.3.phar \
	/usr/local/bin/phpdoc \
	/usr/local/bin/phpunit \
	/usr/local/bin/phpmd
